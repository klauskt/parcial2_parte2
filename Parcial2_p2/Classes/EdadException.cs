﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial2_p2.Classes
{
    class EdadException : Exception
    {
        public EdadException() { }

        public EdadException(string men): base(men) { }

        public EdadException(string men, Exception ex): base(men, ex) { }

        string men = "edad no valida";

        public override string Message {
            get { return men; }
        }
    }
}
