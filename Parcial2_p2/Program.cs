﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parcial2_p2.Classes;

namespace Parcial2_p2
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona persona = new Persona();
            try
            {
                Console.WriteLine("Ingresa nombre: ");
                persona.name = Console.ReadLine();

                Console.WriteLine("Ingresa edad: ");
                persona.edad = Convert.ToInt32(Console.ReadLine());

                if(persona.edad < 0)
                {
                    throw new EdadException();
                }
            }

            catch(EdadException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
